import './cardDescription.css'
import products from "../../../products";
import { useParams } from "react-router-dom";


function CardDescription() {

    const params = useParams();
    const prodId = params.id;
    return (
        <div className="card-desc">
            <img className="card-desc__pic" src={products[+prodId].url} alt="фото" />
            <div className="card-desc__about">
                <h2 className="card-desc__title">{products[+prodId].title}</h2>
                <p className="card-desc__text">{products[+prodId].text}</p>
                <p className="card-desc__price">{products[+prodId].price} ₽</p>
                <p className='card-desc__weight'>{products[+prodId].weight}</p>
            </div>
        </div>
    )
}

export default CardDescription;