import './cardBasket.css'

function CardBasket(props) {
    return (
        <div className="card-b">
            <img className="card-b__pic" src={props.url} alt="" />
            <h2 className="card-b__title">{props.title}</h2>
            <p className="card-b__price">{props.price} ₽</p>
            <button className="card-b__pm" onClick={props.remove}>-</button>
            <p className='card-b__sht'>{props.quantity}шт.</p>
            <button className="card-b__pm" onClick={props.add}>+</button>
            <button className="card-b__btn" onClick={props.removeAll}><img src="/images/delete.svg" alt="Удалить из корзины" /></button>
        </div>
    )
}

export default CardBasket;