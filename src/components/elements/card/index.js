import './card.css'
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";


function Card(props) {

    let navigate = useNavigate();
    const cardChange = (id) => {
        console.log("cardChange id=", id);
        let path = `/products/${id}`;
        navigate(path);
    };

    const basket = useSelector((state) => state.products.basket);

    let itemQuantity = 0;
    for (let i = 0; i < basket.length; i++) {
        if (basket[i].productItem.item.id === props.id) {
            itemQuantity = basket[i].quantity;
            break;
        }
    }

    return (
        <div className="card">
            <img className="card__preview" src={props.url} alt="" />
            <h2 className="card__title" onClick={() => cardChange(props.id)}>{props.title}</h2>
            <p className="card__description">{props.description}</p>
            <p className="card__price">{props.price} ₽</p>
            <p className="card__weight">{props.weight}</p>
            <button className="card__btn" onClick={props.add}>+</button>
            <span className='card__quantity'>{itemQuantity}</span>
            <button className="card__minus" onClick={props.remove}>-</button>
        </div>
    )
}

export default Card;