import './reset.css'

import { Routes, Route } from 'react-router-dom';
import Products from "./Pages/Products/index.js";
import Basket from './Pages/Basket/index.js';
import Entrance from './Pages/Entrance/index.js';
import Registration from './Pages/Registration/index.js'; 
import Description from './Pages/Description/index.js';


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Registration />} />
        <Route path='/entrance' element={<Entrance />} />
        <Route path='/products' element={<Products />} />
        <Route path='/basket' element={<Basket />} />
        <Route path='/products/:id' element={<Description />} />
      </Routes>
    </div>
  );
}

export default App;
