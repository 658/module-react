import { configureStore } from '@reduxjs/toolkit';
import auth from './reduсers/auth'
import products from './reduсers/products';

export const store = configureStore({
    reducer: {
        products: products,
        auth: auth,
    }
});