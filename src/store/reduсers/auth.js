import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    users: [],
    isAuth: false,
    isLoading: false,
};

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
    authRegistration: (state, payload) => {
        state.users.push({
        login: payload.payload.login,
        password: payload.payload.password,
        });
    },
    authLogin: (state, payload) => {
        for (let i = 0; i < state.users.length; i++) {
        if (
            state.users[i].login === payload.payload.login &&
            state.users[i].password === payload.payload.password
        ) {
            state.isAuth = true;
            break;
        }
        }
    },
    authLogout: (state, payload) => {
        state.isAuth = false;
    },
    },
});

export const { authRegistration, authLogin, authLogout } = authSlice.actions;

export default authSlice.reducer;