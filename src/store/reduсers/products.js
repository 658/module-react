import { createSlice } from "@reduxjs/toolkit";

import products from '../../products.js';

const initialState = {
    products: products,
    basket: [],
    page: [],
    countProductInBasket: 0,
    amountProductInBasket: 0,
}

export const productsSlice = createSlice({
    name: "products",
    initialState,
    reducers: {
        addBasket: (state, payload) => {
            
            let flag = false;
            for (let i = 0; i < state.basket.length; i++) {
            console.log(state.basket[i].productItem.item.id);
                if (state.basket[i].productItem.item.id === payload.payload.item.id) {
                    flag = true;
                    state.basket[i].quantity = state.basket[i].quantity + 1;
                    break;
                }
            }
            if (flag === false) {
                state.basket.push({ productItem: payload.payload, quantity: 1 });
            }

            state.countProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.quantity,
                0
            );

            state.amountProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.productItem.item.price * obj.quantity,
                0
            );

        },
        removeBasket: (state, payload) => {
            for (let i = 0; i < state.basket.length; i++) {
                if (state.basket[i].productItem.item.id === payload.payload.item.id) {
                    if (state.basket[i].quantity - 1 > 0) {
                        state.basket[i].quantity = state.basket[i].quantity - 1;
                    } else {
                        state.basket[i].quantity = 0;
                    }
                    break;
                }
            }

            state.countProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.quantity,
                0
            );

            state.amountProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.productItem.item.price * obj.quantity,
                0
            );
        },
        removeWholePosition: (state, payload) => {
            for (let i = 0; i < state.basket.length; i++) {
                if (state.basket[i].productItem.item.id === payload.payload.item.id) {
                    state.basket[i].quantity = 0;
                    break;
                }
            }

            state.countProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.quantity,
                0
            );

            state.amountProductInBasket = state.basket.reduce(
                (acc, obj) => acc + obj.productItem.item.price * obj.quantity,
                0
            );
        },
        addPage: (state, payload) => {
            
            let flag = false;
            for (let i = 0; i < state.page.length; i++) {
            console.log(state.page[i].productItem.item.id);
                if (state.page[i].productItem.item.id === payload.payload.item.id) {
                    flag = true;
                    break;
                }
            }
            if (flag === false) {
                state.page.push({ productItem: payload.payload, quantity: 1 });
            }
        },
    },
});

export const { addBasket } = productsSlice.actions;
export const { removeBasket } = productsSlice.actions;
export const { removeWholePosition } = productsSlice.actions;
export const { addPage } = productsSlice.actions;

export default productsSlice.reducer;