import '../Registration/reg.css';
import './entr.css';
import { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { authRegistration } from "../../store/reduсers/auth";


import { Link, useNavigate } from 'react-router-dom';

function Entrance() {

const dispatch = useDispatch();
const [login, setLogin] = useState('')
const [password, setPassword] = useState('')
const [loginBad, setLoginBad] = useState(false)
const [passwordBad, setPasswordBad] = useState(false)
const [loginError, setLoginError] = useState('')
const [passwordError, setPasswordError] = useState('Поле не должно быть пустым')
const [totalError, setTotalError] = useState('')
const Login = useRef()
const Password = useRef()
const check = useRef()
const [checked, setChecked] = useState(false)

let users = JSON.parse(localStorage.getItem('users'))
    if (users === null) {
        users = []
    }

let navigate = useNavigate();
const routeChange = () => {
    let path = `/products`;
    navigate(path);
};

const handleChange = () => {
    setChecked(!checked)
}

const loginHandler = (e) => {
    setLogin(e.target.value)
    if (e.target.value === null) {
        setLoginError('Поле не должно быть пустым')
    } else {
        setLoginError('')
    }
}

const passwordHandler = (e) => {
    setPassword(e.target.value)
        if (!e.target.value) {
        setPasswordError('Поле не должно быть пустым')
        } else {
        setPasswordError('')
    }
}

const blurHandler = (e) => {
    switch (e.target.name) {
        case 'login':
            setLoginBad(true)
            break
        case 'password':
            setPasswordBad(true)
            break
    }
}

const handleClick = () => {
    users.forEach(item => {
        if (Login.current.value !== item.login || Password.current.value !== item.password) {
            setTotalError('Логин или пароль неверен')
        } else {
            dispatch(authRegistration({ login: item.login, password: item.password }));
            routeChange();
        }
    });
}

    return (
        <div className='entrance'>
            <div className='invisible' id='form_two'>
                <Link to='/' className='link'>Зарегистрироваться</Link>
                <h2 className='form__title'>вход</h2>
                <form name='entr_form' action=''>
                    <input onChange={e => loginHandler(e)} value={login} onBlur={e => blurHandler(e)}  type='text' name='login' required className='form__line' placeholder='Логин' id='login2' ref={Login}/>
                    {(loginBad && loginError) && <p className='error' id='loginError2' style={{marginTop: -15 + 'px', marginBottom: 5 + 'px'}}>{loginError}</p>}
                    <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)} type='password' name='password' required className='form__line' placeholder='Пароль' id='password2' ref={Password} />
                    {(passwordBad && passwordError) && <p className='error' id='passwordError2' style={{marginTop: -15 + 'px', marginBottom: 5 + 'px'}}>{passwordError}</p>}
                </form>
                <div className='form__agreement'>
                    <input type='checkbox' className='form__checkbox' required name='agree' id='agree2' ref={check} checked={checked} onChange={handleChange}/>
                    <label htmlFor='agree2'></label>
                    <p className='checktext'>Я согласен получать обновления на почту</p>
                </div>
                {(totalError) && <p className='totalError' style={{marginTop: 8 + 'px', marginBottom: -18 + 'px'}}>{totalError}</p>}
                <div className='form__button'>
                    <button onClick={handleClick} type='submit' className='button-submit' id='submitEntr'>Войти</button>
                </div>
            </div>
        </div>
    )
}

export default Entrance;