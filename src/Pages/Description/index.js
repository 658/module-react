import './description.css'
import CardDescription from '../../components/elements/cardDescription'

import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { addBasket } from '../../store/reduсers/products';
import { authLogout } from "../../store/reduсers/auth";


function Description() {
    const dispatch = useDispatch()

    const count = useSelector((state) => state.products.countProductInBasket)
    const amount = useSelector((state) => state.products.amountProductInBasket)

    const add = (item) => {
        dispatch(
            addBasket({
            item: item,
            })
        )
    }

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/entrance`;
        navigate(path);
    }

    const outClick = () => {
        dispatch(authLogout());
        routeChange();
    }



    return (
        <div className="description">
            <header className='header'>
                <Link to='/products' className=''><img className='description__img' src="/images/Vector.svg" alt="Стрелка назад" /></Link>
                <p className='description__total'>{count} товаров<br></br>на сумму {amount} ₽</p>
                <Link to='/basket' className=''><img className="description__basket" src="/images/baskey_button.png" alt="Кнопка" /></Link>
                <button onClick={outClick} type='submit' className='description__out'>Выйти</button>
            </header>
            <main>
                (<CardDescription />)
                <button className="description__tobasket" onClick={() => {add(''); navigate(`/products`)}}>В корзину</button>
            </main>
        </div>
    )
}

export default Description;