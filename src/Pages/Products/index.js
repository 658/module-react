import './products.css'
import Card from '../../components/elements/card';
import products from '../../products.js'
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { addBasket } from '../../store/reduсers/products';
import { removeBasket } from '../../store/reduсers/products';
import { authLogout } from "../../store/reduсers/auth";

function Products() {
    const count = useSelector(state => state.products.countProductInBasket)
    const amount = useSelector(state => state.products.amountProductInBasket)
    const basketArray = useSelector((state) => state.products.basket)
    console.log(basketArray);

    const dispatch = useDispatch();
    /*const counter = (event) => {
        dispatch({
            type: event
        })
    }*/
    const add = (item) => {
        dispatch(addBasket({
            item: item
        }))
    }
    const remove = (item) => {
        dispatch(removeBasket({
            item: item
        }))
    }

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/entrance`;
        navigate(path);
    }

    const outClick = () => {
        dispatch(authLogout());
        routeChange();
    }

    return (
        <div className="products">
            <header>
                <h1 className='products__title'>наша продукция</h1>
                <p className='products__total'>{count} товаров<br></br>на сумму {amount} ₽</p>
                <Link to='/basket' className=''><img className="products__basket" src="/images/baskey_button.png" alt="Кнопка" /></Link>
                <button onClick={outClick} type='submit' className='products__out'>Выйти</button>
            </header>
            <main>
                <div className='products__container'>
                    {products.map(item => {
                        return (
                            <Card
                                key={item.id}
                                url={item.url}
                                title={item.title}
                                description={item.description}
                                price={item.price}
                                weight={item.weight}
                                add={() => add(item)}
                                remove={() => remove(item)}
                                id={item.id}
                            />
                        )
                    })}
                </div>
            </main>
        </div>
        
    )
}

export default Products;
