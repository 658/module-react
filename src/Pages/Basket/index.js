import './basket.css'
import CardBasket from '../../components/elements/cardBasket';
import { Link, Outlet, useNavigate } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';

import { addBasket, removeBasket, removeWholePosition } from '../../store/reduсers/products'
import { authLogout } from "../../store/reduсers/auth";

function Basket() {
    const dispatch = useDispatch()

    const basket = useSelector((state) => state.products.basket)
    const count = useSelector((state) => state.products.countProductInBasket)
    const amount = useSelector((state) => state.products.amountProductInBasket)

    const remove = (item) => {
        dispatch(
            removeBasket({
            item: item.productItem.item,
            })
        )
    }
    
    const removeAll = (item) => {
        dispatch(
            removeWholePosition({
            item: item.productItem.item,
            })
        )
    }
    
    const add = (item) => {
        dispatch(
            addBasket({
            item: item.productItem.item,
            })
        )
    }

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/entrance`;
        navigate(path);
    }

    const outClick = () => {
        dispatch(authLogout());
        routeChange();
    }

    return (
        <div className="basket">
            <header className='header'>
                <Link to='/products' className=''><img className='basket__img' src="/images/Vector.svg" alt="Стрелка назад" /></Link>
                <h1 className='basket__title'>корзина с выбранными товарами</h1>
                <button onClick={outClick} type='submit' className='basket__out'>Выйти</button>
            </header>
            <Outlet />
            <main>
                <div className='basket__container'>
                    {basket.map((item) => {
                        if (item.quantity === 0) return null;
                        return (
                            <CardBasket
                                key={item.productItem.item.id}
                                url={item.productItem.item.url}
                                title={item.productItem.item.title}
                                price={item.productItem.item.price}
                                quantity={item.quantity}
                                remove={() => remove(item)}
                                add={() => add(item)}
                                removeAll={() => removeAll(item)}
                            />
                        )
                    })}

                </div>
            </main>
            <footer className='footer'>
                <div className='basket__total'>
                    <h3 className='basket__text'>заказ на сумму:</h3>
                    <p className='basket__amount'>{amount} ₽</p>
                    <p className='basket__sht'>(Количество продуктов в корзине: {count} шт.)</p>
                    <button className='basket__button'>Оформить заказ</button>
                </div>
            </footer>
        </div>
    )
}

export default Basket;
