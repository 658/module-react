import { useEffect, useRef, useState } from 'react';
import './reg.css'
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { authRegistration } from "../../store/reduсers/auth";

let users = JSON.parse(localStorage.getItem('users'));
    if (users === null) {
        users = [];
    }

const Registration = () => {

const dispatch = useDispatch();
const [login, setLogin] = useState('')
const [password, setPassword] = useState('')
const [loginBad, setLoginBad] = useState(false)
const [passwordBad, setPasswordBad] = useState(false)
const [loginError, setLoginError] = useState('Поле не должно быть пустым')
const [passwordError, setPasswordError] = useState('Поле не должно быть пустым')
const Login = useRef()
const Password = useRef()
const check = useRef()
const [checked, setChecked] = useState(false)

let navigate = useNavigate();
const routeChange = () => {
    let path = `/products`;
    navigate(path);
};

const loginHandler = (e) => {
    setLogin(e.target.value)
    const re = /^[a-zA-Z][a-zA-Z0-9-_]{3,15}$/;
    if (!re.test(String(e.target.value).toLowerCase())) {
        setLoginError('Логин должен содержать не менее 4-х символов')
    } else {
        setLoginError('')
    }
}

const passwordHandler = (e) => {
    setPassword(e.target.value)
    if (e.target.value.length < 4 || e.target.value.length > 8) {
        setPasswordError('Пароль должен содержать не менее 4-х символов')
        if (!e.target.value) {
        setPasswordError('Поле не должно быть пустым')
    } 
        } else {
        setPasswordError('')
    }
}

const blurHandler = (e) => {
    switch (e.target.name) {
        case 'login':
            setLoginBad(true)
            break
        case 'password':
            setPasswordBad(true)
            break
    }
}

const handleChange = () => {
    setChecked(!checked)
}

const handleClick = () => {
    const re = /^[a-zA-Z][a-zA-Z0-9-_]{3,15}$/;
    if (re.test(String(Login.current.value).toLowerCase()) && (Password.current.value.length > 4 && Password.current.value.length < 8) && check.current.checked === true) {
        users.push({
            'login': Login.current.value,
            'password': Password.current.value
        })
        dispatch(authRegistration({ login: login, password: password }));
        routeChange();
    }
    localStorage.setItem('users', JSON.stringify(users))
}

    return (
        <div className='entrance'>
            <div className='form_one'>
                <Link to='/entrance' className='link' id='authoriz'>Авторизоваться</Link>
                <h2 className='form__title'>регистрация</h2>
                <form name='contact_form' action=''>
                    <input onChange={e => loginHandler(e)} value={login} onBlur={e => blurHandler(e)} type='text' name='login' required className='form__line' placeholder='Логин' id='login'ref={Login} />
                    {(loginBad && loginError) && <p className='error' id='loginError' style={{marginTop: -15 + 'px', marginBottom: 5 + 'px'}}>{loginError}</p>}
                    <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)} type='password' name='password' required className='form__line' placeholder='Пароль' id='password' ref={Password}/>
                    {(passwordBad && passwordError) && <p className='error' id='passwordError' style={{marginTop: -15 + 'px', marginBottom: 5 + 'px'}}>{passwordError}</p>}
                </form>
                <div className='form__agreement'>
                    <input type='checkbox' className='form__checkbox' required name='agree' id='agree' ref={check} checked={checked} onChange={handleChange} />
                    <label htmlFor='agree'></label>
                    <p className='checktext'>Я согласен получать обновления на почту</p>
                </div>
                <div className='form__button'>
                    <button onClick={handleClick} type='submit' className='button-submit'id='submitReg'>Зарегистрироваться</button>
                </div>
            </div>
        </div>
    )
}


export default Registration;