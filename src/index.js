import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { store } from './store';
import { Provider } from 'react-redux';

import {
  BrowserRouter,
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Basket from './Pages/Basket';

import { createStore } from 'redux';
import Products from './Pages/Products';
import Entrance from './Pages/Entrance';
import Description from './Pages/Description';

const defaultState = {
  count: 0,
  amount: 0
}

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case 'PLUS_COUNT':
      return {
        ...state,
        count: state.count + 1,
        amount: state.amount + 1000
      }
    case 'MINUS_COUNT':
      return {
        ...state,
        count: state.count > 0 ? state.count - 1 : state.count = 0,
        amount: state.amount > 0 ? state.amount - 1000 : state.amount = 0,

      }
    default:
      return state
  }
}

/*const store = createStore(reducer)*/

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/entrance",
        element: <Entrance />,
      },
      {
        path: "/products",
        element: <Products />,
      },
      {
        path: "/basket",
        element: <Basket />,
      },
      {
        path: "/description/:id",
        element: <Description />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      {/*<RouterProvider router={router} />*/}
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
